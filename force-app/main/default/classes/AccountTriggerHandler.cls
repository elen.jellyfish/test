public class AccountTriggerHandler {
    
    public static void handleAfterUpdate(Map<Id, Account> newMap, Map<Id, Account> oldMap){
        Map<Id, AccountCalloutService.AccountWrapper> accountIdToAccountWrapper = new Map<Id, AccountCalloutService.AccountWrapper>();
        for(Account account : newMap.values()){
            if(account.Sum_of_Revenue__c != oldMap.get(account.Id).Sum_of_Revenue__c && account.Sum_of_Revenue__c > 10000){
                accountIdToAccountWrapper.put(account.Id, 
                                              new AccountCalloutService.AccountWrapper(account.Id, 
                                                                                       account.Name, 
                                                                                       account.Sum_of_Revenue__c));
            }
        }
        List<Opportunity> opportunityList = [SELECT Id, Name, Amount, AccountId
                                             FROM Opportunity 
                                             WHERE AccountId IN: accountIdToAccountWrapper.keySet()];
        for(Opportunity opportunity : opportunityList){
            if(accountIdToAccountWrapper.get(opportunity.AccountId).opportunities == null){
                accountIdToAccountWrapper.get(opportunity.AccountId).opportunities = new List<AccountCalloutService.OpportunityWrapper>();
            }
            accountIdToAccountWrapper.get(opportunity.AccountId).opportunities.add(new AccountCalloutService.OpportunityWrapper(opportunity.Id, 
                                                                                                                                opportunity.Name, 
                                                                                                                                opportunity.Amount));
        }
        String accountDataJson = JSON.serialize(new AccountCalloutService.AccountAPICallWrapper(accountIdToAccountWrapper.values()));
        sendCallout(accountDataJson);
    }
    
    @future(callout=true)
    public static void sendCallout(String requestBody){
        HttpResponse response = AccountCalloutService.makePostCallout(requestBody);
        if(response.getStatusCode() >= 200 && response.getStatusCode() < 300){
            String responseJson = response.getBody();
            AccountCalloutService.ResponseWrapper responseWrapper = (AccountCalloutService.ResponseWrapper)JSON.deserialize(response.getBody(), 
                                                                                                                            AccountCalloutService.ResponseWrapper.class);
            if(responseWrapper.accountResponseList == null || responseWrapper.accountResponseList.isEmpty()){
                return;
            }
            Map<Id, String> accountToExternalIdMap = new Map<Id, String>();
            for(AccountCalloutService.AccountResponse accResp : responseWrapper.accountResponseList){
                accountToExternalIdMap.put(accResp.salesforce_id, accResp.record_id);
            }
            List<Account> accountsToUpdate = [SELECT Id 
                                              FROM Account 
                                              WHERE Id IN: accountToExternalIdMap.keySet()];
            for(Account acc : accountsToUpdate){
                acc.External_Id__c = accountToExternalIdMap.get(acc.Id);
            }
            if(!accountsToUpdate.isEmpty()) update accountsToUpdate;
        }
    }
    
}