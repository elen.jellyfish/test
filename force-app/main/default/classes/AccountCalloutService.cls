public class AccountCalloutService {    
    
    private static Account_Callout_Setting__c accCalloutCustomSetting {
        get {
            return accCalloutCustomSetting == null? getCustomSetting() : accCalloutCustomSetting;
        }
        set;
    }
    
    /* There is no information about headers for the request, it is assumed that the necessary data is stored in custom settings, 
     * and authorizationis Bearer token 
	 */
    public static HttpResponse makePostCallout(String requestBody) {
        Http http = new Http();
        HttpRequest request = new HttpRequest();
        request.setEndpoint(accCalloutCustomSetting.Post_endpoint__c);
        request.setMethod('POST');
        request.setHeader('Content-Type', 'application/json;charset=UTF-8');
        request.setHeader('Accept', 'application/json');
        request.setHeader('Authorization', 'Bearer ' + accCalloutCustomSetting.Authorization_token__c);
        request.setBody(requestBody);
        HttpResponse response = http.send(request);
        if (!(response.getStatusCode() >= 200 && response.getStatusCode() < 300)) {
            System.debug(response.getStatusCode() + ' ' + response.getStatus());
        }
        return response;
    }
    
    private static Account_Callout_Setting__c getCustomSetting() {
        return Account_Callout_Setting__c.getInstance() == null? Account_Callout_Setting__c.getOrgDefaults() : Account_Callout_Setting__c.getInstance();
    }

	public class AccountAPICallWrapper{
        public List<AccountWrapper> accounts;
        
        public AccountAPICallWrapper(List<AccountWrapper> accounts){
            this.accounts = accounts;
        }
    }
    
    //It is assumed the request can be sent for the list of Accounts
    public class AccountWrapper{
        public String id;
        public String name;
        public Decimal sum_of_revenue;
        public List<OpportunityWrapper> opportunities;
        
        public AccountWrapper(){}
        
        public AccountWrapper(String id, String name, Decimal sumOfRevenue){
            this.id = id;
            this.name = name;
            this.sum_of_revenue = sumOfRevenue;
        }
    }
    
    public class OpportunityWrapper{
        public String id;
        public String name;
        public Decimal amount;
        
        public OpportunityWrapper(){}
        
        public OpportunityWrapper(String id, String name, Decimal amount){
            this.id = id;
            this.name = name;
            this.amount = amount;
        }
    }    
    
    /* As request can be sent for the list of Accounts, it is assumed that response will be received  
     * as list with SF Id (to match Exteral Id for list of Accounts) and Exteral Id
	 */
    public class ResponseWrapper{
        public List<AccountResponse> accountResponseList;
    }
    
    public class AccountResponse{
        public String salesforce_id;
        public String record_id;
    }

}