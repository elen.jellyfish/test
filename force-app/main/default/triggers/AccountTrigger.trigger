trigger AccountTrigger on Account (before insert, after insert, before update, after update) {
    
    /* It is assumed that callout need to be sent only in case of update, 
     * because it is asked for related list of opportunities
     * which can be created after account being inserted
     */
    if(Trigger.isAfter && Trigger.isUpdate){
        AccountTriggerHandler.handleAfterUpdate(Trigger.newMap, Trigger.oldMap);
    }
    
}